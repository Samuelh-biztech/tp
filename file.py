SERIALPORT = '/dev/ttyACM0'

BAUDRATE = 19200
TIMEOUT = 3

    # pixels with more color value (average for multiple channels) are counted as white
    # tweak this if your images appear too black or too white
black_threshold = 48
    # pixels with less alpha than this are counted as white
alpha_threshold = 127

printer = None

_ESC = chr(27)

import printlib
if __name__ == '__main__':
    
    serialport = '/dev/ttyACMA0'
    p = ThermalPrinter(serialport=serialport)
    p.print_text("\nHello maailma. How's it going?\n")
    p.print_text("Part of this ")
    p.bold()
    p.print_text("line is bold\n")
    p.bold(False)
    p.print_text("Part of this ")
    p.font_b()
    p.print_text("line is fontB\n")
    p.font_b(False)
    p.justify("R")
    p.print_text("right justified\n")
    p.justify("C")
    p.print_text("centered\n")
    p.justify() # justify("L") works too
    p.print_text("left justified\n")
    p.upsidedown()
    p.print_text("upside down\n")
    p.upsidedown(False)

    markup = """bl bold left
ur underline right
fc font b centred (next line blank)
nl
il inverse left
"""
    p.print_markup(markup)

    # runtime dependency on Python Imaging Library
    from PIL import Image
    i = Image.open("example-lammas.png")
    data = list(i.getdata())
    w, h = i.size
    p.print_bitmap(data, w, h, True)
    p.linefeed()
    p.justify("C")
    p.barcode_chr("2")
    p.barcode("014633098808")
    p.linefeed(3)
